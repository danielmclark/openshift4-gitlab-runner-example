FROM docker.io/library/centos:8

COPY update.sh /var/tmp/

ENTRYPOINT ["/bin/bash -c"]

CMD ["/var/tmp/update.sh"]
